﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachObject : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject ObjectToAttach;
    bool objectAttached = false;

    void Start()
    {
        if(ObjectToAttach != null)
        {
            Transform baseAttach = gameObject.transform.GetChild(0).GetChild(0).gameObject.transform;
            if(baseAttach != null)
            {
                GameObject newObj = GameObject.Instantiate<GameObject>(ObjectToAttach);
                newObj.transform.SetParent(baseAttach);
                objectAttached = true;
            }
            
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!objectAttached)
        {
            Transform baseAttach = gameObject.transform.GetChild(0).GetChild(0).gameObject.transform;
            if (baseAttach != null)
            {
                GameObject newObj = GameObject.Instantiate<GameObject>(ObjectToAttach);
                newObj.transform.SetParent(baseAttach);
                newObj.transform.position = new Vector3(0, 0, -0.06f);
                newObj.transform.eulerAngles = new Vector3(-90f, 0, 0);
                objectAttached = true;
            }
        }
    }
}

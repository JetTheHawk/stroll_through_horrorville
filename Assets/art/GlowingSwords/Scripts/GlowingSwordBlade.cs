﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Experimental;

namespace GlowingSwords.Scripts
{
    /// <summary>
    /// Used to animate the switching on and off of the blade or to change the color of the blade.
    /// </summary>
    public class GlowingSwordBlade : MonoBehaviour
    {
        #region Private Members

        private bool _animationRunning = false;
        
        private Color _color = Color.red;
        
        // the light attached to the blade
        private Light _light;


        /// <summary>
        /// Condition whether the blade is activated or deactivated.
        /// </summary>
        private bool _bladeActive = false;

        /// <summary>
        /// The delta is a Lep value within 1 second. 
        /// It is initialized depending on the blade speed.
        /// </summary>
        private float _extendDelta;

        /// <summary>
        /// The local x scale of the blade.
        /// </summary>
        private float _localXScale;
        
        /// <summary>
        /// The local z scale of the blade.
        /// </summary>
        private float _localZScale;
        
        /// <summary>
        /// The min blade scale.
        /// </summary>
        private float _minScale;

        /// <summary>
        /// The max scale of the blade.
        /// </summary>
        private float _maxScale;

        /// <summary>
        /// The current scale of the blade.
        /// </summary>
        private float _scaleCurrent;

        
        #endregion
        
        #region Mesh Renderer

        private MeshRenderer _meshRenderer;

        public MeshRenderer MeshRenderer
        {
            get
            {
                if (_meshRenderer == null)
                    _meshRenderer = gameObject.GetComponent<MeshRenderer>();

                if (_meshRenderer != null) return _meshRenderer;
                
                Debug.LogError("No mesh renderer found.");
                return null;
            }
        }

        #endregion
    
        #region Properties 

        /// <summary>
        /// Property, to activate or deactivate the blade.
        /// </summary>
        public bool BladeActive
        {
            get { return _bladeActive; }
            set
            {
                if (_bladeActive.Equals(value))
                    return;

                _bladeActive = value;
                _extendDelta = _bladeActive ? Mathf.Abs(_extendDelta) : -Mathf.Abs(_extendDelta);
                _animationRunning = true;
            }
        }
        
        /// <summary>
        /// Property for the color of the blade.
        /// </summary>
        public Color Color
        {
            get { return _color; }
            set
            {   
                _color = value;
                
                UpdateLight();

                if (MeshRenderer.materials == null || !MeshRenderer.materials.Any())
                {
                    Debug.LogError("No material found. Please attach the glowing sword blade material.");
                    return;
                }

                MeshRenderer.materials[0].SetColor("_Color", _color);
            }
        }

        #endregion

        #region Setup

        /// <summary>
        /// Setup for the light saber blade.
        /// </summary>
        /// <param name="extendSpeed">The extend speed of the blade.</param>
        /// <param name="active">Activation status of the blade.</param>
        public void Setup(float extendSpeed, bool active)
        {
            _light = gameObject.GetComponentInChildren<Light>();
            _bladeActive = active;

            // consistency check
            if (_light == null)
            {
                Debug.LogWarning(new NullReferenceException(nameof(_light) + " not found."));
                return;
            }

            // remember initial scale values (non extending part of the blade)
            _localXScale = gameObject.transform.localScale.x;
            _localZScale = gameObject.transform.localScale.z;

            // remember initial scale values (extending part of the blade)
            _minScale = 0f;
            _maxScale = gameObject.transform.localScale.y;

            // initialize variables
            // the delta is a lerp value within 1 second. depending on the extend speed we have to size it accordingly
            _extendDelta = _maxScale / extendSpeed;
            
            _extendDelta = active ? Mathf.Abs(_extendDelta) : -Mathf.Abs(_extendDelta);

            ForceSetActive(active);
        }

        #endregion
        
        /// <summary>
        /// Force deactivate the blade
        /// </summary>
        /// <param name="active"></param>
        private void ForceSetActive(bool active)
        {
            UpdateBladeScaleHeight(active?1f:0f);
            gameObject.SetActive(active);
        }

        #region Updates

        private void Update()
        {
            if (_animationRunning == true)
            {
                UpdateSaberSize();
            }
        }
        
        private void UpdateBladeScaleHeight(float deltaScale)
        {
            _scaleCurrent = deltaScale;
            gameObject.transform.localScale = new Vector3(_localXScale, _scaleCurrent, _localZScale);
        }
  
        
        private void UpdateLight()
        {
            if (_light != null)
            {
                _light.color = _color;
            }
        }
        
        public void UpdateSaberSize()
        {
            // consider delta time with blade extension
            _scaleCurrent += _extendDelta * Time.deltaTime;

            // clamp blade size
            _scaleCurrent = Mathf.Clamp(_scaleCurrent, _minScale, _maxScale);

            // scale in z direction
            UpdateBladeScaleHeight(_scaleCurrent);

            // whether the blade is active or not
            _bladeActive = _scaleCurrent > 0;
             
            // show / hide the gameobject depending on the blade active state
            if (_bladeActive && !gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
            else if(!_bladeActive && gameObject.activeSelf)
            {
                gameObject.SetActive(false);
                _animationRunning = false;
            }

            if (_animationRunning && _scaleCurrent >= 1f)
            {
                _animationRunning = false;
            }
        }
        
        /// <summary>
        /// Updates the lighting of the blade.
        /// </summary>
        public void UpdateLighting()
        {
            if (this._light == null)
            {
                //Debug.LogWarning($"{nameof(GlowingSword)}.{nameof(UnityEngine.Experimental.PlayerLoop.Update)}");
                return;
            }

            // light intensity depending on blade size
            this._light.intensity = _scaleCurrent;
        }
        

        #endregion
    }
}
